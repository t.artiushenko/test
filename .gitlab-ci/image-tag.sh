#!/bin/sh

case $CI_COMMIT_REF_NAME in
    dev)
        echo "IMAGE_VERSION=dev-$(date +'%s')" > dot_env
        ;;
    release)
        echo "IMAGE_VERSION=$( ( git tag | egrep rc-[0-9] || echo rc-0 ) \
        | sort -V -r | head -1 | awk -F 'rc-' '{ print "rc-" $2 +1}' )" > dot_env
        ;;
    main)
        echo "IMAGE_VERSION=$( ( git tag | egrep v[0-9]\.[0-9]\.[0-9] || echo v1.0.-1 ) \
        | sort -V -r | head -1 | awk -F. -v OFS=. '{$NF++;print}')" > dot_env
        ;;
esac

if echo $CI_COMMIT_TAG | egrep -q rc-[0-9] || echo $CI_COMMIT_TAG | egrep -q v[0-9]\.[0-9]\.[0-9]; 
then
    echo "IMAGE_VERSION=$CI_COMMIT_TAG" > dot_env;
fi